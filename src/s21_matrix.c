#include "s21_matrix.h"


// Создание матриц (create_matrix)
int s21_create_matrix(int rows, int columns, matrix_t *result) {
    if (rows < 1 || columns < 1) {
        return ERROR;
    }
    result -> rows = rows;
    result -> columns = columns;
    result -> matrix = (double**) malloc(sizeof(double*) * rows);
    for (int i = 0; i < rows; i++) {
        result -> matrix[i] = (double*)malloc(sizeof(double*) * columns);
    }
    return  OK;
}

// Очистка матриц (remove_matrix)
void s21_remove_matrix(matrix_t *A) {
    for (int i = 0; i < A->rows; i++) {
        free(A->matrix[i]);
    }
    free(A -> matrix);
    A -> matrix = NULL;
}

// Сравнение матриц (eq_matrix)
int s21_eq_matrix(matrix_t *A, matrix_t *B) {
    int result = SUCCESS;
    if (!s21_check_quality_matrix(A) || !s21_check_quality_matrix(B) || \
        (A->rows != B->rows) || (A->columns != B->columns)) {
        return FAILURE;
    } else {
        for (int i = 0; i < A->rows; i++) {
            for (int j = 0; j < A->columns; j++) {
                if (A -> matrix[i][j] - B -> matrix[i][j] > 0.0000001 || \
                    B -> matrix[i][j] - A -> matrix[i][j] > 0.0000001) {
                    result = FAILURE;
                }
            }
        }
    }
    return result;
}

// Сложение (sum_matrix)
int s21_sum_matrix(matrix_t *A, matrix_t *B, matrix_t *result) {
    if (!s21_check_quality_matrix(A) || !s21_check_quality_matrix(B)) {
        return ERROR;
    } else if ((A->rows != B->rows) || (A->columns != B->columns)) {
        return ERROR_CAlCUL;
    } else {
        s21_create_matrix(A->rows, A->columns, result);
        for (int i = 0; i < A->rows; i++) {
            for (int j = 0; j < B->columns; j++) {
                result->matrix[i][j] = A->matrix[i][j] + B->matrix[i][j];
            }
        }
    }
    return OK;
}

// вычитание матриц (sub_matrix)
int s21_sub_matrix(matrix_t *A, matrix_t *B, matrix_t *result) {
    if (!s21_check_quality_matrix(A) || !s21_check_quality_matrix(B)) {
        return ERROR;
    } else if ((A->rows != B->rows) || (A->columns != B->columns)) {
        return ERROR_CAlCUL;
    } else {
        s21_create_matrix(A->rows, A->columns, result);
        for (int i = 0; i < A->rows; i++) {
            for (int j = 0; j < B->columns; j++) {
                result -> matrix[i][j] = A->matrix[i][j] - B->matrix[i][j];
            }
        }
    }
    return OK;
}

// Умножение матрицы на число (mult_number)
int s21_mult_number(matrix_t *A, double number, matrix_t *result) {
    if (!s21_check_quality_matrix(A)) {
        return ERROR;
    }
    s21_create_matrix(A->rows, A->columns, result);
    for (int i = 0; i < A->rows; i++) {
        for (int j = 0; j < A->columns; j++) {
            result->matrix[i][j] = A->matrix[i][j] * number;
        }
    }
    return OK;
}

// Умножение двух матриц (mult_matrix)
int s21_mult_matrix(matrix_t *A, matrix_t *B, matrix_t *result) {
    if (!s21_check_quality_matrix(A) || !s21_check_quality_matrix(B)) {
        return ERROR;
    }
    if (A -> columns != B -> rows) {
        return ERROR_CAlCUL;
    }
    s21_create_matrix(A -> rows, B -> columns, result);
    for (int i = 0; i < A -> rows; i++) {
        for (int j = 0; j < B -> columns; j++) {
            result -> matrix[i][j] = 0;
            for (int m = 0; m < B -> rows; m++) {
                result -> matrix[i][j] += A -> matrix[i][m] * B -> matrix[m][j];
            }
        }
    }
    return OK;
}

// Транспонирование матрицы (transpose)
int s21_transpose(matrix_t *A, matrix_t *result) {
    if (!s21_check_quality_matrix(A)) {
        return ERROR;
    }
    s21_create_matrix(A -> columns, A -> rows, result);
    for (int i = 0; i < A -> rows; i++) {
        for (int j = 0; j < A -> columns; j++) {
            result -> matrix[j][i] = A -> matrix[i][j];
        }
    }
    return OK;
}

// Минор матрицы и матрица алгебраических дополнений (calc_complements)
int s21_calc_complements(matrix_t *A, matrix_t *result) {
    int out = OK;
    if (!s21_check_quality_matrix(A)) {
        out = ERROR;
    } else if (A -> rows != A -> columns || A -> rows == 1) {
        out = ERROR_CAlCUL;
    } else {
        s21_create_matrix(A -> rows, A -> columns, result);
        int sign = 1;
        for (int i = 0; i < A->rows; i++) {
            for (int j = 0; j < A->columns; j++) {
                double buff;
                matrix_t temp;
                s21_create_matrix(A -> rows - 1, A -> columns - 1, &temp);
                get_cut_matrix(A, i, j, &temp);
                s21_determinant(&temp, &buff);
                s21_remove_matrix(&temp);
                result -> matrix[i][j] = buff * sign;
                sign = sign * -1;
            }
            if (A -> rows % 2 == 0) {
                sign = sign * -1;
            }
        }
    }
    return out;
}

// Определитель матрицы (determinant)
int s21_determinant(matrix_t *A, double *result) {
    if (!s21_check_quality_matrix(A)) {
        return ERROR;
    } else if (A -> rows != A -> columns) {
        return ERROR_CAlCUL;
    } else if (A -> columns == 1) {
        *result = A -> matrix[0][0];
    } else {
        *result = 0;
        int sign  = 1;
        for (int j = 0; j < A -> columns; j++) {
            double buff = 0;
            matrix_t temp;
            s21_create_matrix(A -> rows - 1, A -> columns - 1, &temp);
            get_cut_matrix(A, 0, j, &temp);
            s21_determinant(&temp, &buff);
            s21_remove_matrix(&temp);
            buff = buff * A -> matrix[0][j] * sign;
            *result += buff;
            sign = sign * -1;
        }
    }
    return OK;
}

// Обратная матрица (inverse_matrix)
int s21_inverse_matrix(matrix_t *A, matrix_t *result) {
    if (!s21_check_quality_matrix(A)) {
        return ERROR;
    } else if (A -> rows != A -> columns) {
        return ERROR_CAlCUL;
    } else {
        double determination = 0;
        s21_create_matrix(A -> rows, A -> columns, result);
        if (s21_determinant(A, &determination) != 0 || fabs(determination - 0) < 0.0000001) {
            return ERROR_CAlCUL;
        } else if (A -> columns == 1) {
            result -> matrix[0][0] = 1.0 / determination;
        } else {
            matrix_t temp;
            s21_calc_complements(A, &temp);
            s21_transpose(&temp, result);
            for (int i = 0; i < temp.rows; i++) {
                for (int j = 0; j < temp.columns; j++) {
                    result -> matrix[i][j] = result -> matrix[i][j] / determination;
                }
            }
            s21_remove_matrix(&temp);
        }
    }
    return OK;
}

/*======================================
        Additional function
=======================================*/

void s21_insert_array(matrix_t *A, double *dum) {
    for (int i = 0; i < A -> rows; i++) {
        for (int j = 0; j < A -> columns; j++) {
            A -> matrix[i][j] = dum[i *  A -> columns + j];
        }
    }
}

int s21_check_quality_matrix(matrix_t *insert) {
    int result = SUCCESS;
    if (insert == NULL || insert->rows < 1 || insert->columns < 1) {
        result = FAILURE;
    }
    return result;
}

/*------------------------
    Cut row and column and save what we remain to result matrix.
    "A" must be quadratic and "Result" must have 1 row and 1 column
    less then "A".
-------------------------*/
void get_cut_matrix(matrix_t *A, int row_A, int col_A, matrix_t *result) {
    int row = 0;
    for (int i = 0; i < A -> rows; i++) {
        int column = 0;
        if (i != row_A) {
            for (int j = 0; j < A -> columns; j++) {
                if (j != col_A) {
                    result -> matrix[row][column++] = A -> matrix[i][j];
                }
            }
            row++;
        }
    }
}
