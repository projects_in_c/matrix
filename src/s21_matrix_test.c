#include <check.h>
#include <stdio.h>
#include <stdlib.h>

#include "s21_matrix.h"

// Create_matrix
START_TEST(s21_create_matrix_1) {
    matrix_t rx_1;
    int result = s21_create_matrix(-1, 2, &rx_1);
    ck_assert_int_eq(result, 1);
}
END_TEST
START_TEST(s21_create_matrix_2) {
    matrix_t rx_1;
    int result = s21_create_matrix(0, 0, &rx_1);
    ck_assert_int_eq(result, 1);
}
END_TEST
START_TEST(s21_create_matrix_3) {
    matrix_t rx_1;
    int result = s21_create_matrix(21, -1, &rx_1);
    ck_assert_int_eq(result, 1);
}
END_TEST
START_TEST(s21_create_matrix_4) {
    matrix_t rx_1;
    int result = s21_create_matrix(9, 8, &rx_1);
    ck_assert_int_eq(result, 0);
    s21_remove_matrix(&rx_1);
}
END_TEST

// Eq_matrix
START_TEST(s21_eq_matrix_1) {
    matrix_t mat_eq_1;
    matrix_t mat_eq_2;
    s21_create_matrix(1, 1, &mat_eq_1);
    mat_eq_1.matrix[0][0] = 1.123;
    s21_create_matrix(1, 1, &mat_eq_2);
    mat_eq_2.matrix[0][0] = 1.123;
    int ret_eq_1 = s21_eq_matrix(&mat_eq_1, &mat_eq_2);
    ck_assert_int_eq(ret_eq_1, 1);
}
END_TEST

// Sum_matrix
START_TEST(s21_sum_matrix_1) {
    matrix_t mat_sum_1_1;
    s21_create_matrix(5, 3, &mat_sum_1_1);
    matrix_t mat_sum_1_2;
    s21_create_matrix(5, 3, &mat_sum_1_2);
    matrix_t res_sum_1_1;
    matrix_t res_sum_1_2;
    s21_create_matrix(5, 3, &res_sum_1_2);
    double dum_sum_1_1[] = {
        -1, -2, -3,
        -4, -5, -6,
        -7, -8, -9,
        -10, -11, -12,
        -0, -0, -0};
    double dum_sum_1_2[] = {
        -1, -2, -3,
        -4, -5, -6,
        -7, -8, -9,
        -10, -11, -12,
        -0, -0, -0};
    double dum_sum_1_3[] = {
        -2, -4, -6,
        -8, -10, -12,
        -14, -16, -18,
        -20, -22, -24,
        -0, -0, -0};
    s21_insert_array(&mat_sum_1_1, dum_sum_1_1);
    s21_insert_array(&mat_sum_1_2, dum_sum_1_2);
    s21_insert_array(&res_sum_1_2, dum_sum_1_3);
    int ret_sum_1 = (s21_sum_matrix(&mat_sum_1_1, &mat_sum_1_2, &res_sum_1_1) == 0
                        && s21_eq_matrix(&res_sum_1_1, &res_sum_1_2));
    ck_assert_int_eq(ret_sum_1, 1);
    s21_remove_matrix(&mat_sum_1_1);
    s21_remove_matrix(&mat_sum_1_2);
    s21_remove_matrix(&res_sum_1_1);
    s21_remove_matrix(&res_sum_1_2);
}
END_TEST

// sub_matrix
START_TEST(s21_sub_matrix_1) {
    matrix_t mat_sub_1_1;
    s21_create_matrix(5, 3, &mat_sub_1_1);
    matrix_t mat_sub_1_2;
    s21_create_matrix(5, 3, &mat_sub_1_2);
    matrix_t res_sub_1_1;
    matrix_t res_sub_1_2;
    s21_create_matrix(5, 3, &res_sub_1_2);
    double dum_sub_1_1[] = {
        -1, -2, -3,
        -4, 5.5, -6,
        -7, -8, -9,
        -10, -11, -12,
        -0, -0, -0};
    double dum_sub_1_2[] = {
        -1, -2, -3,
        -4, -5, -6,
        -7, -8, -9,
        -10, -11, -12,
        -0, -0, -0};
    double dum_sub_1_3[] = {
        -0, -0, -0,
        0, 10.5, -0,
        0, 0, 0,
        0, 0, 0,
        -0, -0, -0};
    s21_insert_array(&mat_sub_1_1, dum_sub_1_1);
    s21_insert_array(&mat_sub_1_2, dum_sub_1_2);
    s21_insert_array(&res_sub_1_2, dum_sub_1_3);
    int ret_sub_1 = (s21_sub_matrix(&mat_sub_1_1, &mat_sub_1_2, &res_sub_1_1) == 0
                        && s21_eq_matrix(&res_sub_1_1, &res_sub_1_2) == 1);
    ck_assert_int_eq(ret_sub_1, 1);
    s21_remove_matrix(&mat_sub_1_1);
    s21_remove_matrix(&mat_sub_1_2);
    s21_remove_matrix(&res_sub_1_1);
    s21_remove_matrix(&res_sub_1_2);
}
END_TEST

// mult_number
START_TEST(s21_mult_number_1) {
    matrix_t mat_muln_1_1;
    s21_create_matrix(5, 3, &mat_muln_1_1);
    double number = 2.0;
    matrix_t res_muln_1_1;
    matrix_t res_muln_1_2;
    s21_create_matrix(5, 3, &res_muln_1_2);
    double dum_muln_1_1[] = {
        -1, -2, -3,
        -4, 5.5, -6,
        -7, -8, -9,
        -10, -11, -12,
        -0, -0, -0};

    double dum_muln_1_3[] = {
        -2, -4, -6,
        -8, 11, -12,
        -14, -16, -18,
        -20, -22, -24,
        -0, -0, -0};
    s21_insert_array(&mat_muln_1_1, dum_muln_1_1);
    s21_insert_array(&res_muln_1_2, dum_muln_1_3);
    int ret_muln_1 = (s21_mult_number(&mat_muln_1_1, number, &res_muln_1_1) == 0
                        && s21_eq_matrix(&res_muln_1_1, &res_muln_1_2) == 1);
    ck_assert_int_eq(ret_muln_1, 1);
    s21_remove_matrix(&mat_muln_1_1);
    s21_remove_matrix(&res_muln_1_1);
    s21_remove_matrix(&res_muln_1_2);
}
END_TEST

// Mult_matrix
START_TEST(s21_mult_matrix_1) {
    matrix_t mat_mul_1_1;
    s21_create_matrix(5, 3, &mat_mul_1_1);
    matrix_t mat_mul_1_2;
    s21_create_matrix(3, 6, &mat_mul_1_2);
    matrix_t res_mul_1_1;
    matrix_t res_mul_1_2;
    s21_create_matrix(5, 6, &res_mul_1_2);
    double dum_mul_1_1[] = {
        -1, -2, -3,
        -4, 5.5, -6,
        -7, -8, -9,
        -10, -11, -12,
        -0, -0, -0};
    double dum_mul_1_2[] = {
        -1, -2, -3, 4.5, 4.6, 4.7,
        -4, -5, -6, 6.7, 6.8, 6.9,
        -7, -8, -9, 1.1, 1.2, 1.3};
    double dum_mul_1_3[] = {
        30, 36, 42, -21.2, -21.8, -22.4,
        24, 28.5, 33, 12.25, 11.8, 11.35,
        102, 126, 150, -95, -97.4, -99.8,
        138, 171, 204, -131.9, -135.2, -138.5,
        0, 0, 0, 0, 0, 0};
    s21_insert_array(&mat_mul_1_1, dum_mul_1_1);
    s21_insert_array(&mat_mul_1_2, dum_mul_1_2);
    s21_insert_array(&res_mul_1_2, dum_mul_1_3);
    int ret_mul_1 = (s21_mult_matrix(&mat_mul_1_1, &mat_mul_1_2, &res_mul_1_1) == 0
                        && s21_eq_matrix(&res_mul_1_1, &res_mul_1_2) == 1);
    ck_assert_int_eq(ret_mul_1, 1);
    s21_remove_matrix(&mat_mul_1_1);
    s21_remove_matrix(&mat_mul_1_2);
    s21_remove_matrix(&res_mul_1_1);
    s21_remove_matrix(&res_mul_1_2);
}
END_TEST

// Determinant
START_TEST(s21_determinant_1) {
matrix_t mat_det_1_1;
s21_create_matrix(3, 3, &mat_det_1_1);
double result_det_1_1 = 0;
double result_det_1_2 = -126;

double dum_det_1_1[] = {
    -1, -2, -3,
    -4, 5.5, -6,
    -7, -8, -9};
s21_insert_array(&mat_det_1_1, dum_det_1_1);
int ret_det_1 = (s21_determinant(&mat_det_1_1, &result_det_1_1) == 0
                    && fabs(result_det_1_1 - result_det_1_2) < 0.0000001);
ck_assert_int_eq(ret_det_1, 1);
s21_remove_matrix(&mat_det_1_1);
}
END_TEST

START_TEST(s21_determinant_2) {
matrix_t mat_det_4_1;
s21_create_matrix(4, 3, &mat_det_4_1);
double result_det_4_1 = 0;
double dum_det_4_1[] = {
    0, 0, 0,
    -4, 5.5, -6,
    -7, -8, -9,
    1, 2, 3};
s21_insert_array(&mat_det_4_1, dum_det_4_1);
int ret_det_4 = (s21_determinant(&mat_det_4_1, &result_det_4_1) == 2);
ck_assert_int_eq(ret_det_4, 1);
s21_remove_matrix(&mat_det_4_1);
}
END_TEST

START_TEST(complements_1) {
matrix_t mat_com_1_1;
s21_create_matrix(3, 3, &mat_com_1_1);
matrix_t res_com_1_1;
matrix_t res_com_1_2;
s21_create_matrix(3, 3, &res_com_1_2);
double dum_com_1_1[] = {
    1, 2, 3,
    0, 4, 2,
    5, 2, 1};
double dum_com_1_3[] = {
    0, 10, -20,
    4, -14, 8,
    -8, -2, 4};
s21_insert_array(&mat_com_1_1, dum_com_1_1);
s21_insert_array(&res_com_1_2, dum_com_1_3);
int ret_com_1 = (s21_calc_complements(&mat_com_1_1, &res_com_1_1) == 0
                    && s21_eq_matrix(&res_com_1_1, &res_com_1_2) == 1);
ck_assert_int_eq(ret_com_1, 1);
s21_remove_matrix(&mat_com_1_1);
s21_remove_matrix(&res_com_1_1);
s21_remove_matrix(&res_com_1_2);
}
END_TEST

START_TEST(complements_2) {
matrix_t mat_com_2_1;
s21_create_matrix(1, 1, &mat_com_2_1);
matrix_t res_com_2_1;
matrix_t res_com_2_2;
s21_create_matrix(1, 1, &res_com_2_2);
double dum_com_2_1[] = {
    1};
double dum_com_2_3[] = {
    1};
s21_insert_array(&mat_com_2_1, dum_com_2_1);
s21_insert_array(&res_com_2_2, dum_com_2_3);
int ret_com_2 = (s21_calc_complements(&mat_com_2_1, &res_com_2_1) == 2);
ck_assert_int_eq(ret_com_2, 1);
s21_remove_matrix(&mat_com_2_1);
s21_remove_matrix(&res_com_2_2);
}
END_TEST

START_TEST(complements_3) {
    matrix_t a, result_1, result_2;
    s21_create_matrix(3, 3, &a);
    s21_create_matrix(3, 3, &result_1);
    double A[3][3] = {{1, 22, 333}, {-0.1, 5.5, 0}, {123, 9, 5.5}};
    double r[3][3] = {{30.25, 0.55, -677.4}, {2876, -40953.5, 2697}, {-1831.5, -33.3, 7.7}};
    for (int k = 0; k < 3; k++) {
        for (int g = 0; g < 3; g++) {
            a.matrix[k][g] = A[k][g];
            result_1.matrix[k][g] = r[k][g];
        }
    }
    int k = s21_calc_complements(&a, &result_2);
    double det = 0;
    int d = s21_determinant(&a, &det);
    ck_assert_int_eq(k, 0);
    ck_assert_int_eq(d, 0);
    ck_assert_double_eq_tol(det,  -4510637.0 / 20.0, 1e-6);
    ck_assert_int_eq(s21_eq_matrix(&result_1, &result_2), 1);
    s21_remove_matrix(&a);
    s21_remove_matrix(&result_1);
    s21_remove_matrix(&result_2);
}
END_TEST

START_TEST(complements_4) {
    matrix_t a, result_1, result_2;
    s21_create_matrix(2, 2, &a);
    s21_create_matrix(2, 2, &result_1);
    double A[2][2] = {{1, 2}, {3, 4}};
    double r[2][2] = {{4, -3}, {-2, 1}};
    for (int k = 0; k < 2; k++) {
        for (int g = 0; g < 2; g++) {
            a.matrix[k][g] = A[k][g];
            result_1.matrix[k][g] = r[k][g];
        }
    }
    int k = s21_calc_complements(&a, &result_2);
    double det = 0;
    int d = s21_determinant(&a, &det);
    ck_assert_int_eq(k, 0);
    ck_assert_int_eq(d, 0);
    ck_assert_double_eq_tol(det,  -2, 1e-6);
    ck_assert_int_eq(s21_eq_matrix(&result_1, &result_2), 1);
    s21_remove_matrix(&a);
    s21_remove_matrix(&result_1);
    s21_remove_matrix(&result_2);
}
END_TEST

START_TEST(complements_5) {
    matrix_t a, result_1, result_2;
    s21_create_matrix(4, 4, &a);
    s21_create_matrix(4, 4, &result_1);
    double A[4][4] = {{9, -2222, 0, 1000}, {6, 5, 467, 3}, {12, 23, 34, 45}, {-1, 2, 3, -4}};
    double r[4][4] = {{83642, 15, -936, -21605}, {-603162, 67561, -38696, 155549}, \
                    {3251694, -468107, -29896, -1069399}, {57039686, -5211783, -599352, -12093939}};
    for (int k = 0; k < 4; k++) {
        for (int g = 0; g < 4; g++) {
            a.matrix[k][g] = A[k][g];
            result_1.matrix[k][g] = r[k][g];
        }
    }
    int k = s21_calc_complements(&a, &result_2);
    double det = 0;
    int d = s21_determinant(&a, &det);
    ck_assert_int_eq(k, 0);
    ck_assert_int_eq(d, 0);
    ck_assert_double_eq_tol(det, -20885552.0, 1e-6);
    ck_assert_int_eq(s21_eq_matrix(&result_1, &result_2), 1);
    s21_remove_matrix(&a);
    s21_remove_matrix(&result_1);
    s21_remove_matrix(&result_2);
}
END_TEST

START_TEST(s21_inverse_matrix_1) {
matrix_t mat_inv_1_1;
s21_create_matrix(3, 3, &mat_inv_1_1);
matrix_t res_inv_1_1;
matrix_t res_inv_1_2;
s21_create_matrix(3, 3, &res_inv_1_2);
double dum_inv_1_1[] = {
    2, 5, 7,
    6, 3, 4,
    5, -2, -3};
double dum_inv_1_3[] = {
    1, -1, 1,
    -38, 41, -34,
    27, -29, 24};
s21_insert_array(&mat_inv_1_1, dum_inv_1_1);
s21_insert_array(&res_inv_1_2, dum_inv_1_3);
int ret_inv_1 = (s21_inverse_matrix(&mat_inv_1_1, &res_inv_1_1) == 0
                    && s21_eq_matrix(&res_inv_1_1, &res_inv_1_2) == 1);
ck_assert_int_eq(ret_inv_1, 1);
s21_remove_matrix(&mat_inv_1_1);
s21_remove_matrix(&res_inv_1_1);
s21_remove_matrix(&res_inv_1_2);
}
END_TEST

START_TEST(s21_inverse_matrix_2) {
    matrix_t a, result_1, result_2;
    s21_create_matrix(2, 2, &a);
    s21_create_matrix(2, 2, &result_1);
    double A[2][2] = {{100, -54.45}, {411, 0}};
    double r[2][2] = {{0, 1.0 / 411.0}, {-20.0 / 1089.0, 2000.0 / 447579.0}};
    for (int k = 0; k < 2; k++) {
        for (int g = 0; g < 2; g++) {
            a.matrix[k][g] = A[k][g];
            result_1.matrix[k][g] = r[k][g];
        }
    }
    int k = s21_inverse_matrix(&a, &result_2);
    ck_assert_int_eq(k, 0);
    ck_assert_int_eq(s21_eq_matrix(&result_1, &result_2), 1);
    s21_remove_matrix(&a);
    s21_remove_matrix(&result_1);
    s21_remove_matrix(&result_2);
}
END_TEST

START_TEST(s21_inverse_matrix_3) {
    matrix_t a, result_1, result_2;
    double l = 11121;
    s21_create_matrix(3, 3, &a);
    s21_create_matrix(3, 3, &result_1);
    double A[3][3] = {{1, 2, 3}, {4, 5, 6}, {1234, 5678, -999}};
    double r[3][3] = {{-13021.0 / l, 6344.0 / l, -1.0 / l}, {3800.0 / l, -1567 / l, 2.0 / l}, \
                    {1838.0 / 3707.0, -1070.0 / l, -1.0 / l}};
    for (int k = 0; k < 3; k++) {
        for (int g = 0; g < 3; g++) {
            a.matrix[k][g] = A[k][g];
            result_1.matrix[k][g] = r[k][g];
        }
    }
    int k = s21_inverse_matrix(&a, &result_2);
    ck_assert_int_eq(k, 0);
    ck_assert_int_eq(s21_eq_matrix(&result_1, &result_2), 1);
    s21_remove_matrix(&a);
    s21_remove_matrix(&result_1);
    s21_remove_matrix(&result_2);
}
END_TEST

START_TEST(error_1) {
matrix_t invalid;
s21_create_matrix(2, 2, &invalid);
invalid.rows = -1;
double result = 0;
int ret = (s21_sum_matrix(&invalid, &invalid, &invalid) == 1 &&
           s21_sub_matrix(&invalid, &invalid, &invalid) == 1 &&
           s21_mult_matrix(&invalid, &invalid, &invalid) == 1 &&
           s21_mult_number(&invalid, 2, &invalid) == 1 &&
           s21_transpose(&invalid, &invalid) == 1 &&
           s21_determinant(&invalid, &result) == 1 &&
           s21_calc_complements(&invalid, &invalid) == 1 &&
           s21_inverse_matrix(&invalid, &invalid) == 1);
ck_assert_int_eq(ret, 1);}
END_TEST

int main() {
    Suite *s1 = suite_create("Create_matrix");
    TCase *s21_matrix_tests_1 = tcase_create("Tests");
    suite_add_tcase(s1, s21_matrix_tests_1);
    tcase_add_test(s21_matrix_tests_1, s21_create_matrix_1);
    tcase_add_test(s21_matrix_tests_1, s21_create_matrix_2);
    tcase_add_test(s21_matrix_tests_1, s21_create_matrix_3);
    tcase_add_test(s21_matrix_tests_1, s21_create_matrix_4);

    Suite *s2 = suite_create("Eq_matrix");
    TCase *s21_matrix_tests_2 = tcase_create("Tests");
    suite_add_tcase(s2, s21_matrix_tests_2);
    tcase_add_test(s21_matrix_tests_2, s21_eq_matrix_1);
    // tcase_add_test(s21_matrix_tests_2, s21_eq_matrix_2);
    // tcase_add_test(s21_matrix_tests_2, s21_eq_matrix_3);
    // tcase_add_test(s21_matrix_tests_2, s21_eq_matrix_4);

    Suite *s3 = suite_create("Sum_matrix");
    TCase *s21_matrix_tests_3 = tcase_create("Tests");
    suite_add_tcase(s3, s21_matrix_tests_3);
    tcase_add_test(s21_matrix_tests_3, s21_sum_matrix_1);

    Suite *s4 = suite_create("Sub_matrix");
    TCase *s21_matrix_tests_4 = tcase_create("Tests");
    suite_add_tcase(s4, s21_matrix_tests_4);
    tcase_add_test(s21_matrix_tests_4, s21_sub_matrix_1);

    Suite *s5 = suite_create("Mult_number");
    TCase *s21_matrix_tests_5 = tcase_create("Tests");
    suite_add_tcase(s5, s21_matrix_tests_5);
    tcase_add_test(s21_matrix_tests_5, s21_mult_number_1);

    Suite *s6 = suite_create("Mult_matrix");
    TCase *s21_matrix_tests_6 = tcase_create("Tests");
    suite_add_tcase(s6, s21_matrix_tests_6);
    tcase_add_test(s21_matrix_tests_6, s21_mult_matrix_1);

    Suite *s7 = suite_create("Determinant");
    TCase *s21_matrix_tests_7 = tcase_create("Tests");
    suite_add_tcase(s7, s21_matrix_tests_7);
    tcase_add_test(s21_matrix_tests_7, s21_determinant_1);
    tcase_add_test(s21_matrix_tests_7, s21_determinant_2);

    Suite *s8 = suite_create("Calc_complements");
    TCase *s21_matrix_tests_8 = tcase_create("Tests");
    suite_add_tcase(s8, s21_matrix_tests_8);
    tcase_add_test(s21_matrix_tests_8, complements_1);
    tcase_add_test(s21_matrix_tests_8, complements_2);
    tcase_add_test(s21_matrix_tests_8, complements_3);
    tcase_add_test(s21_matrix_tests_8, complements_4);
    tcase_add_test(s21_matrix_tests_8, complements_5);

    Suite *s9 = suite_create("Inverse_matrix");
    TCase *s21_matrix_tests_9 = tcase_create("Tests");
    suite_add_tcase(s9, s21_matrix_tests_9);
    tcase_add_test(s21_matrix_tests_9, s21_inverse_matrix_1);
    tcase_add_test(s21_matrix_tests_9, s21_inverse_matrix_2);
    tcase_add_test(s21_matrix_tests_9, s21_inverse_matrix_3);
    tcase_add_test(s21_matrix_tests_9, error_1);

    SRunner *runner_1 = srunner_create(s1);
    SRunner *runner_2 = srunner_create(s2);
    SRunner *runner_3 = srunner_create(s3);
    SRunner *runner_4 = srunner_create(s4);
    SRunner *runner_5 = srunner_create(s5);
    SRunner *runner_6 = srunner_create(s6);
    SRunner *runner_7 = srunner_create(s7);
    SRunner *runner_8  = srunner_create(s8);
    SRunner *runner_9  = srunner_create(s9);

    srunner_run_all(runner_1, CK_NORMAL);
    srunner_run_all(runner_2, CK_NORMAL);
    srunner_run_all(runner_3, CK_NORMAL);
    srunner_run_all(runner_4, CK_NORMAL);
    srunner_run_all(runner_5, CK_NORMAL);
    srunner_run_all(runner_6, CK_NORMAL);
    srunner_run_all(runner_7, CK_NORMAL);
    srunner_run_all(runner_8, CK_NORMAL);
    srunner_run_all(runner_9, CK_NORMAL);

    srunner_free(runner_1);
    srunner_free(runner_2);
    srunner_free(runner_3);
    srunner_free(runner_4);
    srunner_free(runner_5);
    srunner_free(runner_6);
    srunner_free(runner_7);
    srunner_free(runner_8);
    srunner_free(runner_9);
    return 0;
}
