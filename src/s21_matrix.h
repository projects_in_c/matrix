#ifndef SRC_S21_MATRIX_H_
#define SRC_S21_MATRIX_H_

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define FAILURE 0;
#define SUCCESS 1;

#define OK 0;
#define ERROR 1;
#define ERROR_CAlCUL 2;

typedef struct matrix_struct {
    double** matrix;
    int rows;
    int columns;
} matrix_t;

/*======================================
        Corr function
=======================================*/
// Создание матриц (create_matrix)
int s21_create_matrix(int rows, int columns, matrix_t *result);

// Очистка матриц (remove_matrix)
void s21_remove_matrix(matrix_t *A);

// Сравнение матриц (eq_matrix)
int s21_eq_matrix(matrix_t *A, matrix_t *B);

// Сложение (sum_matrix)
int s21_sum_matrix(matrix_t *A, matrix_t *B, matrix_t *result);

// вычитание матриц (sub_matrix)
int s21_sub_matrix(matrix_t *A, matrix_t *B, matrix_t *result);

// Умножение матрицы на число (mult_number)
int s21_mult_number(matrix_t *A, double number, matrix_t *result);

// Умножение двух матриц (mult_matrix)
int s21_mult_matrix(matrix_t *A, matrix_t *B, matrix_t *result);

// Транспонирование матрицы (transpose)
int s21_transpose(matrix_t *A, matrix_t *result);

// Минор матрицы и матрица алгебраических дополнений (calc_complements)
int s21_calc_complements(matrix_t *A, matrix_t *result);

// Определитель матрицы (determinant)
int s21_determinant(matrix_t *A, double *result);

// Обратная матрица (inverse_matrix)
int s21_inverse_matrix(matrix_t *A, matrix_t *result);

/*======================================
        Additional function
=======================================*/
int s21_check_quality_matrix(matrix_t *insert);
void s21_insert_array(matrix_t *A, double *dum);
void get_cut_matrix(matrix_t *A, int row_A, int col_A, matrix_t *result);

#endif  // SRC_S21_MATRIX_H_
